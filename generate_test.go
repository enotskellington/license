package license

import "testing"

type testPair struct {
	values Values
	lic    string
}

var (
	testSetCaseSensitivity = []testPair{
		{Values{Name: "robbie"}, "mit"},
		{Values{Name: "bubba"}, "MiT"},
	}
	testSetNotFound = []testPair{
		{Values{Name: "robbie"}, "boo"},
		{Values{Name: "jack horseman"}, "golang"},
	}
	testSetTooFew = []testPair{
		{Values{}, "mit"},
		{Values{Name: "bo peep"}, "gpl"},
	}
)

// TestGenCaseSensitivity should not return an error
func TestGenCaseSensitivity(t *testing.T) {
	for _, pair := range testSetCaseSensitivity {
		_, err := Gen(pair.lic, pair.values)
		if err != nil {
			t.Error("Gen returned an unexpected error")
		}
	}
}

// TestGenTooFewArgs should return error ErrTooFewArgs
func TestGenTooFewArgs(t *testing.T) {
	for _, pair := range testSetTooFew {
		_, err := Gen(pair.lic, pair.values)
		if err != ErrTooFewArgs {
			t.Errorf("Gen should return %v: instead got %v", ErrTooFewArgs, err)
		}
	}
}

// TestGenNotFound should return error ErrNotFound
func TestGenNotFound(t *testing.T) {
	for _, pair := range testSetNotFound {
		_, err := Gen(pair.lic, pair.values)
		if err != ErrNotFound {
			t.Errorf("Gen should return %v: instead got %v", ErrNotFound, err)
		}
	}
}

// TestNew confirms success of creating custom license template
func TestNew(t *testing.T) {
	testNeeds := map[string]bool{
		"Name": true,
	}
	err := New("TESTLICENSE", `this is a {{.Name}}`, "short desc", testNeeds)
	if err != nil {
		t.Errorf("New returned an unexpected error: %v", err)
	}
	if _, ok := Templates["TESTLICENSE"]; !ok {
		t.Error("New failed to insert new license template")
	}
	testInput := Values{
		Name: "tested license",
	}
	tempTmpl, err := Gen("TESTLICENSE", testInput)
	if err != nil {
		t.Errorf("New returned an unexpected error: %v", err)
	}
	if tempTmpl != `this is a tested license` {
		t.Error("Template generated from test does not match expected string")
	}
}

// TestNewError should return error ErrAlreadyExists
func TestNewError(t *testing.T) {
	err := New("MIT", "", "", make(map[string]bool))
	if err != ErrAlreadyExists {
		t.Error("New should not attempt to insert a license whose id exists")
	}
}

// TestShow should return a string == to the matching entry in templates
func TestShow(t *testing.T) {
	tmpl, err := Show("unlicense")
	if tmpl != Templates["UNLICENSE"].Tmpl {
		t.Error("Show returned the wrong template string")
	} else if err != nil {
		t.Error("Show returned an unexpected error")
	}
}
