License
=======

A software license generating library written in Go.

`import "rjl.li/license"`

API
---

```

// Gen takes a string, t, stating the type of license template to use.
// Values, d, a struct containing the strings to replace template elements.
// Returned is the full, formatted license or an error.
func Gen(t string, d Values) (string, error)

// Show takes a string, t, stating the type of license template to use.
// Returned is the whole template literal string or an error.
func Show(t string) (string, error)

// New takes three strings: n, t, d. Name, Template and Description.
// map[string]bool, v, which should have a key that is true for each element in the template. See Values in data.Go
// Returned is an error if any, no error denotes success.
func New(n, t, d string, v map[string]bool) error

```


© 2017  Rob J loranger  [hello@robloranger.ca](mailto://hello@robloranger.ca)